# Databse about films
> This database was created to test MySQL function connected to PHP

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshot](#screenshot)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Contact](#contact)


## General Information
- This project was realized on V semester of studies.
- The view is implemented in PHP t oserve SQL database
- The purpose of project was to learn SQL comand line and PHP language.


## Technologies Used
- MySQL
- PHP v5
- HTML (view)


## Features
Provided features:
- Sorting films by categories
- Showing details about films
- Editing positions
- full CRUD build
- Showing only directors




## Project Status
Project is: _no longer being worked on_. 


## Room for Improvement
To do:
- rewrite PHP code in OOP
- Make code clear.
- Make a view nicer



## Contact
Created by Piotr Hajduk

